﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Group : MonoBehaviour {
    // Time since last gravity tick 
    private float nextActionTime = 0.0f;
    public float period = 1.0f;
    public int score = 0;

    // Use this for initialization
    void Start () {
        if(!isValidGridPos())
         {
            Debug.Log("GAME OVER");
            Destroy(gameObject); 
        }
		
	}

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            transform.position += new Vector3(-1, 0, 0);

            //is it valid?
            if (isValidGridPos())
                updateGrid();
            else
                transform.position += new Vector3(1, 0, 0);
        }
        else if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            transform.position += new Vector3(1, 0, 0);
            if (isValidGridPos())
                updateGrid();
            else
                transform.position += new Vector3(-1, 0, 0);
        }
        else if (Input.GetKeyDown(KeyCode.UpArrow) || Input.GetKeyDown(KeyCode.C))
        {
            rotateShape(transform);
            //transform.Rotate(0, 0, -90);
            if (isValidGridPos())
                updateGrid();
            else
                transform.Rotate(0, 0, 90);
        }
        else if (Input.GetKeyDown(KeyCode.DownArrow) ||
         Time.time > nextActionTime)
        {
            transform.position += new Vector3(0, -1, 0);
            if (isValidGridPos())
                updateGrid();
            else
            {
                transform.position += new Vector3(0, 1, 0);
                Grid.deleteFullRows();
                FindObjectOfType<Spawner>().spawnNext();
                enabled = false;
            }
            nextActionTime = Time.time + period;

        }

        
    }

    void rotateShape(Transform shape)
    {
        Transform pivot = shape.FindChild("pivot"); //each shape has a pivot block (the center of the block for rotations)
        if (shape.name != "shapeO")//dont do anything because O shape doesnt rotate
        {
            Vector2 p = Grid.roundVec2(pivot.position);

            foreach (Transform child in transform)
            {
                if (child != pivot)
                {
                    Vector2 v = Grid.roundVec2(child.position);
                    Vector2 vr = v - p; //relative vector to pivot
                    Vector2 vf = new Vector2(-vr.y, vr.x); //rotate this relative vector by 90 degrees, clockwise
                    Vector2 vnew = vf + p; //add this vector to the pivot. block has a new rotated position around pivot
                    child.position = new Vector3(vnew.x, vnew.y, 0); // set this block to the new position
                }
            }
        }
    }

    bool isValidGridPos()
    {
        foreach (Transform child in transform)
        {
            Vector2 v = Grid.roundVec2(child.position); 
            
            //Not inside border? 
            if(!Grid.insideBorder(v))
                return false;

            //BLock in grid cell ( and not part of same group)? 
            if (Grid.grid[(int)v.x, (int)v.y] != null &&
                Grid.grid[(int)v.x, (int)v.y].parent != transform)
                return false;
        }
        return true; 
    }

    void updateGrid()
    {
        for (int y = 0; y < Grid.h; ++y)
        {
            for (int x = 0; x < Grid.w; ++x)
            {
                if (Grid.grid[x, y] != null)
                    if (Grid.grid[x, y].parent == transform)
                        Grid.grid[x, y] = null; 
            }
        }

        //add new children to grid
        foreach (Transform child in transform)
        {
            Vector2 v = Grid.roundVec2(child.position);
            Grid.grid[(int)v.x, (int)v.y] = child; 
 
        }
    }
    


}
