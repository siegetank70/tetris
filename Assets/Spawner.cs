﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{

    public GameObject[] groups;

    void Start()
    {
        spawnNext();
    }

    public void spawnNext()
    {
        int i = Random.Range(0, groups.Length);
        GameObject spawn = Instantiate(groups[i], transform.position,
                    Quaternion.identity);
        spawn.name = spawn.name.Replace("(Clone)", "");
    }

}
